<?php

if(isset($_POST["submit"])){
    

    //Variables
    $name=$_POST["name"];
    $username= $_POST["userName"];
    $pwd=$_POST["pwd"];
    $pwdRepeat= $_POST["repwd"];


    //Connect To Other Files
    require_once 'connect.php';
    require_once 'functions-inc.php';

    
    #region Error Handling

    //User Left A Input Box Empty
    if(emptyInputSignup($name, $pwd, $pwdRepeat)!==false){
        header("location: register.html?error=emptyinput");     //links to back to register page withe an error message 
        exit();                                                 //exits current php page
    }

    //User Username is invalid
    if(invalidUid($username)!==false){
        header("location: register.html?error=invalidUid");
        exit();
    }

    //Password Does Not Match
    if(pwdMatch($pwd,$pwdRepeat)!==false){
        header("location: register.html?error=passwordDontMatch");
        exit();
    }

    //Username Already used in database
    if(uidExists($conn,$username)!==false){
        header("location: register.html?error=usernamealreadytaken");
        exit();
    }

    #endregion Error Handling


    //Creates New User !
    createUser($conn,$name,$username,$pwd);
}
else {
    header("location: register.html?error=cantconnect");
    exit();
}