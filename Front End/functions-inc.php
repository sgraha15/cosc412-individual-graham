<?php

//Empty Input Box
function emptyInputSignup($name, $pwd, $pwdRepeat){
    $result;
    if(empty($name)||empty($pwd)||empty($pwdRepeat)){
        $result = true ;
        
    }
    else{
        $result = false;
    }
    return $result;
}

//checks Input Boxes are empty 
function emptyInputLogin($username, $pwd){

    $result;
    if(empty($username)||empty($pwd)){
        $result = true;
        
    }
    else{
        $result = false;
    }
    return $result;
}

//Checks if user login is valid
function loginUser($conn, $username, $pwd){
    $uidExists = uidExists($conn, $username);
    
    if($uidExists=false){
        header("location: Login.html?error=wronglogin1");
        exit();

    }
    

    $pwdHashed = uidExists($conn, $username)["userPwd"];
    $checkPwd= password_verify($pwd,$pwdHashed);

    if($checkPwd === false){
        
        header("location: Login.html?error=wronglogin2");
        exit();
    }
    else if($checkPwd === true){
        session_start();
        $_SESSION["userid"]= $uidExists["userId"];
        $_SESSION["userName"]=$uidExists["userName"];
        header("location: index.html");
        exit();
    }

}


//Username has correct Characters
function invalidUid($username){
    $result;

    if(!preg_match("/^[a-zA-Z0-9]*$/",$username))      //Checks If Valid Username
    { 
        $result = true ;
    }
    else{
        $result = false;
    }

    return $result;
}

//Checks If Passwords Match
function pwdMatch($pwd,$pwdRepeat){

    $result;
    if ($pwd !== $pwdRepeat){
        $result = true;
    }
    else{
        $result = false;
    }

    return $result;
}

//Checks if Username exist in the database
function uidExists($conn,$username){
    $sql = "SELECT * FROM user WHERE userUid = ?;";   // Search Database 
    $stmt = mysqli_stmt_init($conn);                   //Prepared Statment Variable (stmt=statement)
    if(!mysqli_stmt_prepare($stmt,$sql))                     //Prepared Statement
    {   
        header("location: register.html?error=stmtfailed");
        exit();
    }

    mysqli_stmt_bind_param($stmt,"s",$username);        //Binds username to statment ("s"= # strings if #=2="ss")
    mysqli_stmt_execute($stmt);                         //Executes Statement(stmt)

    $resultData = mysqli_stmt_get_result($stmt);

    //check if any data is returned (checks if username is used already)
    if($row=mysqli_fetch_assoc($resultData))                 //fetch resultdata as an assoc array 
    {   
        return $row;
    }
    else
    {
        $result = false;
        return $result;
    }

    mysqli_stmt_close($stmt);


}

//creates User
function createUser($conn,$name,$username,$pwd){
    $sql = "INSERT INTO user(userName, userUid, userPwd) VALUES(?,?,?);";   // Insert Into DataBase
    $stmt = mysqli_stmt_init($conn); 
                                                             //Prepared Statment Variable (stmt=statement)
    if(!mysqli_stmt_prepare($stmt,$sql))                     //Prepared Statement
    {   
        header("location: register.html?error=stmtfailed");
        exit();
    }

    $hashedPwd = password_hash($pwd, PASSWORD_DEFAULT);                //hashes password

    mysqli_stmt_bind_param($stmt,"sss",$name,$username,$hashedPwd);        //Binds username to statment ("s"= # strings if #=2="ss")
    mysqli_stmt_execute($stmt);
    mysqli_stmt_close($stmt);
    header("location: register.html?error=none");
    exit();
}